package net.bor.lightelevator;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class LightElevator extends JavaPlugin {

    public static LightElevator instance;

    private File configFile = null;
    private FileConfiguration config = null;


    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();

        getServer().getPluginManager().registerEvents(new EventListener(), this);

    }

    @Override
    public void onDisable() {
    }

    @Override
    public void reloadConfig() {
        if (configFile == null) {
            configFile = new File(getDataFolder(), "config.yml");
        }
        config = YamlConfiguration.loadConfiguration(configFile);
        if (configFile != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(configFile);
            config.setDefaults(defConfig);
        }
    }

    @Override
    public FileConfiguration getConfig() {
        if (config == null) {
            reloadConfig();
        }
        return config;
    }

    @Override
    public void saveConfig() {
        if (config != null && configFile != null) {
            try {
                getConfig().save(configFile);
            } catch (IOException ex) {
                getServer().getConsoleSender().sendMessage(ChatColor.RED + this.getName() + "Could not save config to " + configFile);
            }
        }
    }

    @Override
    public void saveDefaultConfig() {
        if (configFile == null) {
            configFile = new File(getDataFolder(), "config.yml");
        }
        if (!configFile.exists()) {
            saveResource("config.yml", false);

            getConfig().set("Enable", true);

            saveConfig();
            reloadConfig();
            getServer().getConsoleSender().sendMessage(this.getName() + "Default config.yml saved.");
        }
    }

}
