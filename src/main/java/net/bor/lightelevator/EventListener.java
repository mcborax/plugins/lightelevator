package net.bor.lightelevator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Beacon;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EventListener implements Listener {

    private List<Material> glass = new LinkedList<>();

    public EventListener() {
        glass.add(Material.GLASS);
        glass.add(Material.WHITE_STAINED_GLASS);
        glass.add(Material.ORANGE_STAINED_GLASS);
        glass.add(Material.MAGENTA_STAINED_GLASS);
        glass.add(Material.LIGHT_BLUE_STAINED_GLASS);
        glass.add(Material.YELLOW_STAINED_GLASS);
        glass.add(Material.LIME_STAINED_GLASS);
        glass.add(Material.PINK_STAINED_GLASS);
        glass.add(Material.GRAY_STAINED_GLASS);
        glass.add(Material.LIGHT_GRAY_STAINED_GLASS);
        glass.add(Material.CYAN_STAINED_GLASS);
        glass.add(Material.PURPLE_STAINED_GLASS);
        glass.add(Material.BLUE_STAINED_GLASS);
        glass.add(Material.BROWN_STAINED_GLASS);
        glass.add(Material.GREEN_STAINED_GLASS);
        glass.add(Material.RED_STAINED_GLASS);
        glass.add(Material.BLACK_STAINED_GLASS);
    }

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        if (event.isSneaking()) {
            elevat(event.getPlayer(), false);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (player.getVelocity().getY() > 0.4 && !player.isSneaking()) {
            elevat(player, true);
        }
    }

    private void elevat(Player player, boolean isUp) {
        if ((isUp && player.getLocation().getBlockY() + 5 > player.getWorld().getMaxHeight()) || (!isUp && player.getLocation().getBlockY() - 5 < 0)) {
            return;
        }
        if (player.getVehicle() != null || !glass.contains(player.getLocation().add(0, -1, 0).getBlock().getType())) {
            return;
        }
        List<Location> locs = new ArrayList<>();
        getDownLevel(player.getLocation().add(0, 2, 0), 0, locs);
        if (!locs.isEmpty()) {
            if (isUp) {
                locs.clear();
                getNextUpLevel(player.getLocation(), 1, locs);
                if (locs.size() > 1) {
                    Location loc = locs.get(1).add(0, 1, 0);
                    teleport(player, loc);
                    loc.getWorld().playSound(loc, Sound.BLOCK_BEACON_ACTIVATE, 2, 5);
                }
            } else if (locs.size() > 1) {
                Location loc = locs.get(1).add(0, 1, 0);
                teleport(player, loc);
                loc.getWorld().playSound(loc, Sound.BLOCK_BEACON_DEACTIVATE, 2, 5);
            }
        }
    }

    private void teleport(Player player, Location loc) {
        player.getWorld().getNearbyEntities(player.getLocation(), 1, 1, 1, entity -> {
            if (entity.getType().isAlive() && !(entity instanceof ItemFrame) && !(entity instanceof ArmorStand) && entity.getVehicle() == null) {
                Location newLoc = entity.getLocation().clone();
                newLoc.setY(loc.getY());
                entity.teleport(newLoc);
            }
            return false;
        });
        player.teleport(loc);
    }

    private void getNextUpLevel(Location loc, int countAir, List<Location> level) {
        if (loc.getWorld() == null || loc.getBlockY() + 5 > loc.getWorld().getMaxHeight()) {
            return;
        }
        Location up = loc.add(0, 1, 0);
        if (up.getBlock().getType().isAir()) {
            if (countAir == 1) {
                level.add(up.clone().add(0, -2, 0));
            }
            getNextUpLevel(up, countAir + 1, level);
        } else if (glass.contains(up.getBlock().getType())) {
            getNextUpLevel(up, 0, level);
        } else {
            level.clear();
        }
    }

    private void getDownLevel(Location loc, int countAir, List<Location> level) {
        if (loc.getWorld() == null || loc.getBlockY() + 5 > loc.getWorld().getMaxHeight() || loc.getBlockY() - 5 < 0) {
            level.clear();
            return;
        }
        Location down = loc.add(0, -1, 0);
        if (down.getBlock().getType().isAir()) {
            getDownLevel(down, countAir + 1, level);
        } else if (glass.contains(down.getBlock().getType())) {
            if (countAir > 1) {
                level.add(down.clone());
            }
            getDownLevel(down, 0, level);
        } else if (down.getBlock().getType() == Material.BEACON) {
            if (((Beacon) down.getBlock().getState()).getTier() > 0) {
                return;
            }
        } else {
            level.clear();
        }
    }

}
